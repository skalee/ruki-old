# -*- encoding: utf-8 -*-
require File.expand_path('../lib/ruki/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Sebastian Ska\305\202acki"]
  gem.email         = ["skalee@gmail.com"]
  gem.description   = %q{TODO: Write a gem description}
  gem.summary       = %q{MediaWiki access api in Ruby}
  gem.homepage      = ""

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "ruki"
  gem.require_paths = ["lib"]
  gem.version       = Ruki::VERSION

  gem.add_dependency "rest-client", "=1.6.7"
  gem.add_dependency "json_pure", "~>1.6"

  gem.add_development_dependency "cucumber", "~>1.1.9"
  gem.add_development_dependency "rspec-core", "~>2.10"
  gem.add_development_dependency "rspec-expectations", "~>2.10"
  gem.add_development_dependency "aruba", "~> 0.4.11"
  gem.add_development_dependency "sequel", "~> 3.35"
  gem.add_development_dependency "mysql"
  #gem.add_development_dependency "database_cleaner", "~>0.7.2" dependency moved to Gemfile; stock version is buggy, we need one from git (https://github.com/bmabey/database_cleaner/pull/121)
  gem.add_development_dependency "ffaker"
end
