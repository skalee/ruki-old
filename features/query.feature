Feature: Using Raw Query Interface
  Scenario: Retrieving single property through Wiki.query
    Given following script where API_URL represents API endpoint of wiki:
        """
        wiki = Ruki::Wiki.new(API_URL)
        resp = wiki.query(:meta => :siteinfo, :siprop => :statistics)
        stats = resp['query']['statistics']
        puts "users: #{stats['users']}, articles: #{stats['articles']}"
        """
    When I execute it
    Then the output should match /\Ausers: \d+, articles: \d+\Z/

