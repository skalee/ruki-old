require 'aruba/cucumber'
require File.join(File.dirname(__FILE__), '..', '..', 'spec', 'support', 'common_init')

begin
  require 'database_cleaner'
  require 'database_cleaner/cucumber'
  #DatabaseCleaner[:sequel].strategy = :truncation
  DatabaseCleaner[:sequel].strategy = :transaction
rescue NameError
  raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
end

Before do
  DatabaseCleaner.start
end

After do
  DatabaseCleaner.clean
end
