Given /^article entitled "(.*)" with body "(.*)"$/ do |title, body|
  manufacture_page(:title => title, :body => body)
end

Then /^there should be an article entitled "([^"]*)" with body "([^"]*)"$/ do |title, body|
  page = DB[:page].where(:page_title => title, :page_namespace => 0).first
  page.should_not be_nil
  actual_body = DB[:revision].join(:text, :old_id => :rev_text_id).where(:rev_id => page[:page_latest])[:old_text]
  actual_body.should == body
end

