Given /^following script(?: where API_URL represents API endpoint of wiki)?:$/ do |file_content|
  script = ["API_URL = '#{API_URL}'", file_content].join("\n")
  write_file("script.rb", script)
end

When /^I execute it$/ do
  run_simple(unescape("ruby -I '#{File.join File.dirname(__FILE__), '..', '..', 'lib'}' -r 'ruki' script.rb"), false)
end

