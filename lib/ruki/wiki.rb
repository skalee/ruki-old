require 'rest_client'
require 'json/pure'

module Ruki
  class Wiki
    attr_reader :api_url

    def initialize(api_url)
      @api_url = api_url
    end

    def query *args
      if args.length == 1 and args[0].kind_of? Hash
        self.request args[0]
      else
        raise
      end
    end

    def ensure_login
      return true unless @cookies.nil?
    end

    def request params
      ensure_login
      [:titles].each do |param_to_escape|
        params[param_to_escape] = CGI::escape(params[param_to_escape]) if params[param_to_escape]
      end
      params[:format] ||= 'json'
      params[:action] ||= 'query'

      response = RestClient.get @api_url, :params => params, :user_agent => Ruki::USER_AGENT, :cookies => @cookies
      JSON.parse(response)
    end

    def [] title
      Page.new self, title
    end
  end
end

