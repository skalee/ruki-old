module Ruki
  class Page
    attr_reader :title
    attr_reader :id
    #attr_reader :changed?
    attr_accessor :text
    attr_reader :wiki

    @changed = false
    @fetched = false

    def id
      fetch ; @id
    end

    def text
      fetch
      @text.nil? ? '' : @text
    end

    def missing?
      fetch ; @id.nil?
    end

    def initialize(wiki, title)
      @wiki = wiki
      @title = title
    end

    protected

    def fetch
      fetch! unless @fetched
    end

    def fetch!
      response = wiki.query(:titles => title, :prop => 'revisions', :rvprop => 'ids|timestamp|content')
      page_info = response['query']['pages'].values.first
      @id ||= page_info['pageid']
      @title ||= page_info['title']
      @text = @id ? page_info['revisions'].first['*'] : nil
      @fetched = true
    end
  end
end

