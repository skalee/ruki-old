require "ruki/version"

module Ruki
  USER_AGENT = "Ruki v. #{VERSION}"

  autoload :Wiki, 'ruki/wiki'
  autoload :Page, 'ruki/page'
end

