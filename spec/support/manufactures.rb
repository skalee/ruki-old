require 'ffaker'
require 'digest/sha1'

#TODO possible pitfall, insert generally returns PK of new record, but this may be false on some RDBMSes.

def manufacture_page options
  body = options[:text] || Faker::Lorem.paragraph
  title = options[:title] || Faker::Name.name
  body_sha1 = Digest::SHA1.hexdigest(body).to_i(16).to_s(36)

  text_id = DB[:text].insert(:old_text => body)
  page_id = DB[:page].insert(:page_title => title, :page_namespace => 0)
  rev_id = DB[:revision].insert(:rev_page => page_id, :rev_text_id => text_id, :rev_comment => Faker::Lorem.sentence, :rev_parent_id => 0, :rev_len => body.length, :rev_user => '0', :rev_user_text => Faker::Internet.ip_v4_address, :rev_timestamp => "20120525165041", :rev_sha1 => body_sha1)
  DB[:page].where(:page_id => page_id).update(:page_latest => rev_id)
end
