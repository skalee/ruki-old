require 'rspec/expectations'

RSpec::Matchers.define :respond_returning do |expected_value|
  description{ |z| "respond returning #{expected_value.inspect} to #{@fun}" }

  match do |subj|
    (@responds_at_all = subj.respond_to?(@fun)) and ((@actual_value = subj.public_send(@fun)) == expected_value)
  end

  chain :to do |fun|
    @fun = fun
  end

  failure_message_for_should do |subj|
    unless @responds_at_all
      "expected #{subj.inspect} to respond to #{@fun}"
    else
      "expected #{subj.inspect} to return #{expected_value.inspect} in the response to #{@fun}, but #{@actual_value.inspect} was returned instead"
    end
  end
end

