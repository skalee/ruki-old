require 'sequel'

ROOT = File.join(File.dirname(__FILE__), '..', '..')

begin
  config = YAML::load(IO.read(File.join ROOT, 'test_configuration.yml'))
  API_URL = config['api_url']
  DB = Sequel.mysql(config['database']['database'], :user => config['database']['user'], :password => config['database']['password'])
rescue Errno::ENOENT
  log(:fatal, "database.yml with configuration for connecting test data base couldn't be found. CRASH."); return
end

require File.join(ROOT, 'spec', 'support', 'manufactures.rb')

