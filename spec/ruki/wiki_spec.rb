require 'spec_helper'

module Ruki
  describe Wiki do
    let(:wiki){ Wiki.new $API_URL }
    #describe '#articles' do
    #  it "should return ArticleList"
    #end
    describe "[] operator" do
      #TODO it should be moved to mixin, this met. may be useful in article lists too.
      context "when page 'Raven' does exist" do
        it "should return an instance of Page of title Raven" do
          pending
          page = wiki["anything"]
          page.should be_a Page
          page.title.should == "anything"
        end
      end
      context "when page 'Poe' does not exist" do
        it "should return an instance of Page of title 'Edgar Alan Poe'" do
          page = wiki["Edgar Alan Poe"]
          page.should be_a Page
          page.title.should == "Edgar Alan Poe"
        end
      end
    end
  end
end

