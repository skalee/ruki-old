require 'spec_helper'

module Ruki
  describe "non-existing page entitled 'Edgar Alan Poe'" do
    let(:wiki){ Wiki.new API_URL }
    subject{ Page.new wiki, "Edgar Alan Poe" }

    it{ should respond_returning('Edgar Alan Poe').to(:title) }
    it{ should be_missing }
    it{ should respond_returning(nil).to(:id) }
    it{ should respond_returning('').to(:text) }
    it{ should respond_to :wiki }
  end

  describe "existing page entitled 'Raven'" do
    let(:wiki){ Wiki.new API_URL }
    before{ @page_id = manufacture_page(:title => "Raven", :text => "Deep into that darkness peering, long I stood there wondering, fearing") }
    subject{ Page.new wiki, "Raven" }

    it{ should respond_returning('Raven').to(:title) }
    it{ should_not be_missing }
    it{ should respond_returning(@page_id).to(:id) }
    it{ should respond_returning('Deep into that darkness peering, long I stood there wondering, fearing').to(:text) }
    it{ should respond_to :wiki }
  end

  describe "existing page entitled 'Black Cat'" do
    it "should be fetched properly even though has special characters in title"
  end
end

